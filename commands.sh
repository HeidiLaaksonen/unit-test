# --> tekee kommentin
#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-        luo kansiot projects kansioon
code ~/projects/stam/L03/unit-test/       code avaa tuon kansion

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

git remote add origin https://gitlab.com/HeidiLaaksonen/unit-test
# git remote set-url origin     // jos haluaa vaihtaa yllä olevan osoitteen
git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

node src/main.js
node --watch-path=src src/main.js
npm run dev
npm run test

# test files that goes to staging area
git add . --dry-run